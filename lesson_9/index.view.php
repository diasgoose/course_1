<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Document</title>
    </head>
    <body>
<h1>Task for the day</h1>
    <ul>
        <li>
     <strong>Name of the task: </strong> <?= $task["title"] ?>
    </li>
    </ul>
    <ul>
        <li>
            <strong>Due date: </strong> <?=$task["due"] ?>
        </li>
    </ul>
    <ul>
        <li>
            <strong>Assigned to: </strong> <?=$task["assigned_to"] ?>
        </li>
    </ul>
<ul>
<li>
    <strong>Status: </strong>
    <?php if ($task['completed']) : ?>
<span class="icon">&#9989;</span>
<?php else : ?>
        <span class="icon">Incomplete</span>
<?php endif; ?>
</li></ul>
    <ul>
        <li>
            <strong>Took more than 30 minutes?: </strong>
            <?php if ($task['took more than 30 minutes?']) : ?>
            <span class="icon">Yes</span>
            <?php else : ?>
                <span class="icon">No</span>
                <?php endif;?>
        </li>
    </ul>
    </body>
    </html>