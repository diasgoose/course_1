<?php
require "functions.php";
class Task {
        public $description;
        public $completed = false;
    public function __construct($description)
    {
        $this->description = $description;
    }
    public function iscomplete()
    {
        return $this->completed;
    }
    public function complete()
    {
        $this->completed = true;
    }
}


$tasks = [
  new Task("Go to the store"),
    new Task("Finish my setup"),
    new Task("Clean my room")
];

$tasks [0]->complete();

$task = new Task("Go to the store");
require 'index.view.php ';