<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Document</title>
    </head>
    <body>
<h1>Task for the day</h1>
    <ul>
        <li>
     <strong>Name of the task: </strong> <?= $task["title"] ?>
    </li>
    </ul>
    <ul>
        <li>
            <strong>Due date: </strong> <?=$task["due"] ?>
        </li>
    </ul>
    <ul>
        <li>
            <strong>Assigned to: </strong> <?=$task["assigned_to"] ?>
        </li>
    </ul>
<ul>
<li>
    <strong>Status: </strong> <?=$task["completed"] ? "Complete" : "Incomplete"; ?>
</li></ul>
    </body>
    </html>